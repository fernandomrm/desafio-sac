# Desafio SAC

A aplicação consiste de duas partes, backend e frontend.

*backend*: Implementado utilizando Python3 e Django REST framework. Para servir a aplicação, foi utilizado o Gunicorn.

*frontend*: Implementado utilizando React, Redux e Lodash. O Webpack foi utilizado para realizar o build da aplicação e
o NPM para gerenciar as dependências.

# Setup do projeto

Dependências do projeto: Python 3.4 e Node v4.4.4

Clonar repositório `git clone git@bitbucket.org:fernandomrm/desafio-sac.git`

Entrar no diretório do projeto criado `cd desafio-sac`

Instala dependências e construir ambiente `make setup`

Criar atendente `make create-attendant`

# Executar projeto

Executar no terminal `make run`

Acessar: [localhost:8000](http://localhost:8000)


# Rodar testes

`make test`

# PEP8
Checar se o código está de acordo com a PEP8: `make pep8`

# Descrição da API

`POST /api/login`
```json
{
    "username": "username",
    "password": "password"
}
```
Cria e retorna o token do atendente.

`POST /api/logout`

Remove o token do atendente.

`GET /api/tickets`

Lista todos os tickets.

`GET /api/tickets/<id>`

Retorna o ticket `<id>`.

`POST /api/tickets/`
```json
{
	"state": "RJ",
    "category": "Chat",
    "motive": "Elogios",
    "description": "description"
}
```
Cria um ticket vinculado ao atendente logado e retorna o ticket criado.

`PUT /api/tickets/<id>`
```json
{
	"state": "RJ",
    "category": "Chat",
    "motive": "Sugestões",
    "description": "description"
}
```
Atualiza as informações do ticket `<id>` e retorna o ticket atualizado.

`DELETE /api/tickets/<id>`

Exlui o ticket `<id>`.

`GET /api/attendant-tickets`

Retorna os tickets do atendente logado.
