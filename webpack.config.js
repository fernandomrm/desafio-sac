var webpack = require('webpack');

module.exports = function() {
    process.env.API_URL = process.env.API_URL || 'http://localhost:8000/api';

    var config = {
        entry: './frontend/src/index.js',
        output: {
            path: './frontend/static',
            filename: 'bundle.js'
        },
        plugins : [
            new webpack.EnvironmentPlugin(['NODE_ENV', 'API_URL']),
        ],
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    include: __dirname + '/frontend/src',
                    loader: 'babel'
                }
            ]
        }
    };

    if (process.env.NODE_ENV == 'production') {
        config.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                compress: {warnings: true}
            })
        )
    }
    else {
        config.devtool = 'source-map'
    }
    return config
}();
