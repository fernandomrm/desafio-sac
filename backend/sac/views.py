from django.http import Http404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView


from .models import Ticket
from .serializers import TicketSerializer


class TicketView(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, ticket_id=None):
        if ticket_id:
            try:
                ticket = Ticket.objects.get(id=ticket_id)
                serializer = TicketSerializer(ticket)
            except Ticket.DoesNotExist:
                raise Http404
        else:
            tickets = Ticket.objects.all()
            serializer = TicketSerializer(tickets, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = TicketSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(attendant=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, ticket_id):
        try:
            ticket = Ticket.objects.get(id=ticket_id, attendant=request.user)
        except Ticket.DoesNotExist:
            raise Http404
        serializer = TicketSerializer(ticket, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, ticket_id):
        try:
            ticket = Ticket.objects.get(id=ticket_id, attendant=request.user)
        except Ticket.DoesNotExist:
            raise Http404
        ticket.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AttendantTicketsView(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        tickets = request.user.ticket_set.all()
        serializer = TicketSerializer(tickets, many=True)
        return Response(serializer.data)


class LogoutView(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
