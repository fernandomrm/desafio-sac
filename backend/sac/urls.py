from django.conf.urls import url
from rest_framework.authtoken import views

from .views import TicketView, AttendantTicketsView, LogoutView

urlpatterns = [
    url(r'^tickets/$', TicketView.as_view(), name='tickets'),
    url(r'^tickets/(?P<ticket_id>\d+)/$', TicketView.as_view(), name='tickets'),
    url(r'^attendant-tickets/$', AttendantTicketsView.as_view(), name='attendant-tickets'),
    url(r'^login/$', views.obtain_auth_token),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
]
