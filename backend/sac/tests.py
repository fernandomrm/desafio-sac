from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from .factories import TicketFactory, AttendantFactory
from .models import Ticket


class SACTestCase(APITestCase):

    def setUp(self):
        self.attendant = AttendantFactory()

    def _get_data(self, **kwargs):
        data = {
            'category': 'Telefone',
            'state': 'RJ',
            'motive': 'Dúvidas',
            'description': 'Description'
        }
        data.update(kwargs)
        return data

    def test_list_all_tickets(self):
        self.client.force_authenticate(user=self.attendant)
        TicketFactory.create_batch(5)
        url = reverse('tickets')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)

    def test_do_not_access_tickets_without_attentication(self):
        TicketFactory.create_batch(5)
        url = reverse('tickets')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_ticket_by_id(self):
        self.client.force_authenticate(user=self.attendant)
        ticket = TicketFactory()
        url = reverse('tickets', args=[ticket.id])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], ticket.id)

    def test_ticket_not_found(self):
        self.client.force_authenticate(user=self.attendant)
        url = reverse('tickets', args=[7])
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_ticket(self):
        self.client.force_authenticate(user=self.attendant)
        url = reverse('tickets')
        response = self.client.post(url, self._get_data())

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Ticket.objects.count(), 1)
        self.assertEqual(Ticket.objects.get().attendant, self.attendant)

    def test_do_not_create_ticket_if_not_is_valid(self):
        self.client.force_authenticate(user=self.attendant)
        url = reverse('tickets')
        response = self.client.post(url, self._get_data(category=''))

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Ticket.objects.count(), 0)

    def test_update_ticket(self):
        self.client.force_authenticate(user=self.attendant)
        ticket = TicketFactory(attendant=self.attendant)
        url = reverse('tickets', args=[ticket.id])
        response = self.client.put(url, self._get_data(motive='Sugestões'))
        ticket.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Ticket.objects.count(), 1)
        self.assertEqual(ticket.motive, 'Sugestões')

    def test_do_not_update_ticket_if_attendant_different_than_authenticated(self):
        self.client.force_authenticate(user=self.attendant)
        ticket = TicketFactory()
        url = reverse('tickets', args=[ticket.id])
        response = self.client.put(url, self._get_data(motive='Sugestões'))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_ticket(self):
        self.client.force_authenticate(user=self.attendant)
        ticket = TicketFactory(attendant=self.attendant)
        url = reverse('tickets', args=[ticket.id])
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Ticket.objects.count(), 0)

    def test_do_not_delete_ticket_if_attendant_different_than_authenticated(self):
        self.client.force_authenticate(user=self.attendant)
        ticket = TicketFactory()
        url = reverse('tickets', args=[ticket.id])
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(Ticket.objects.count(), 1)

    def test_list_tickets_of_the_attendant(self):
        self.client.force_authenticate(user=self.attendant)
        TicketFactory.create_batch(2)
        TicketFactory.create_batch(3, attendant=self.attendant)
        url = reverse('attendant-tickets')
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_logout(self):
        Token.objects.create(user=self.attendant)
        self.client.force_authenticate(user=self.attendant)
        url = reverse('logout')
        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Token.objects.count(), 0)
