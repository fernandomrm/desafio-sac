import factory
from factory.django import DjangoModelFactory

from .models import Attendant, Ticket


class AttendantFactory(DjangoModelFactory):
    class Meta:
        model = Attendant

    email = factory.Sequence(lambda n: 'attendant{0}@mail.com'.format(n))
    password = factory.Faker('password')


class TicketFactory(DjangoModelFactory):
    class Meta:
        model = Ticket

    attendant = factory.SubFactory(AttendantFactory)
    category = 'Telefone'
    state = 'RJ'
    motive = 'Dúvidas'
    description = factory.Faker('text')
