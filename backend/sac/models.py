from datetime import datetime

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class MyUserManager(BaseUserManager):
    def create_user(self, email, password):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.save()
        return user


class Attendant(AbstractBaseUser):
    email = models.EmailField(max_length=254, unique=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'


class Ticket(models.Model):
    CATEGORY_CHOICES = (
        ('Telefone', 'Telefone'),
        ('Chat', 'Chat'),
        ('Email', 'Email')
    )

    STATE_CHOICES = (
        ('AC', 'Acre'),
        ('AL', 'Alagoas'),
        ('AP', 'Amapá'),
        ('AM', 'Amazonas'),
        ('BA', 'Bahia'),
        ('CE', 'Ceará'),
        ('ES', 'Espírito Santo'),
        ('GO', 'Goiás'),
        ('MA', 'Maranhão'),
        ('MT', 'Mato Grosso'),
        ('MS', 'Mato Grosso do Sul'),
        ('MG', 'Minas Gerais'),
        ('PA', 'Pará'),
        ('PB', 'Paraíba'),
        ('PR', 'Paraná'),
        ('PE', 'Pernambuco'),
        ('PI', 'Piauí'),
        ('RJ', 'Rio de Janeiro'),
        ('RN', 'Rio Grande do Norte'),
        ('RS', 'Rio Grande do Sul'),
        ('RO', 'Rondônia'),
        ('RR', 'Roraima'),
        ('SC', 'Santa Catarina'),
        ('SP', 'São Paulo'),
        ('SE', 'Sergipe'),
        ('TO', 'Tocantins'),
        ('DF', 'Distrito Federal'),
    )

    MOTIVE_CHOICES = (
        ('Dúvidas', 'Dúvidas'),
        ('Elogios', 'Elogios'),
        ('Sugestões', 'Sugestões'),
    )

    attendant = models.ForeignKey('Attendant')
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=10)
    state = models.CharField(choices=STATE_CHOICES, max_length=2)
    motive = models.CharField(choices=MOTIVE_CHOICES, max_length=20)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    @property
    def formatted_date(self):
        return datetime.strftime(self.created, '%d/%m/%Y')
