.SILENT:

PYTHON=venv/bin/python3
PIP=venv/bin/pip
FLAKE8=venv/bin/flake8
GUNICORN=venv/bin/gunicorn

build-backend:
	$(info Construindo dependências do backend)
	virtualenv venv -p python3 --clear
	${PIP} install -U pip
	${PIP} install -r requirements-dev.txt
	cd backend && ../${PYTHON} manage.py migrate

build-frontend:
	$(info Construindo dependências do frontend)
	npm install

setup: build-backend build-frontend
	@echo OK

test-backend:
	cd backend && ../${PYTHON} manage.py test --settings=backend.settings.test

test-frontend:
	npm test

test: test-backend test-frontend

run:
	npm run build && cp -rf frontend/static backend/sac/
	cd backend && ../${PYTHON} manage.py collectstatic --noinput
	cd backend && ../${GUNICORN} -w 4 --access-logfile=- backend.wsgi:application

pep8:
	${FLAKE8} --max-line-length=119 --exclude=venv/,backend/sac/migrations . || true

clear-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

create-attendant:
	cd backend && ../${PYTHON} manage.py createsuperuser
