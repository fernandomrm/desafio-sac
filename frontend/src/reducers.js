import {RECEIVE_TICKETS, LOGIN, LOGOUT, CHANGE_PAGE} from './actions';


export function tickets(state = [], action) {
    switch (action.type) {
        case RECEIVE_TICKETS:
            return action.tickets;
        default:
            return state
    }
}

export function user(state = {}, action) {
    switch (action.type) {
        case LOGIN:
            return action.user;
        case LOGOUT:
            return {};
        default:
            return state
    }
}

export function activePage(state = '', action) {
    switch (action.type) {
        case CHANGE_PAGE:
            return action.page;
        default:
            return state
    }
}
