import React, {PropTypes, Component} from 'react'
import _ from 'lodash';

import store from './store';
import {
    saveTicket,
    requestTickets,
    requestAttendantTickets,
    login,
    deleteTicket
} from './actions';


export class LoginPage extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            errorMessage: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        const {username, password} = this.state;
        if (username && password) {
            store.dispatch(login({username, password}))
                .catch(error => {
                    let errorMessage = error.message;
                    if (error.response.status == 400) {
                        errorMessage = 'E-mail ou senha incorreto';
                    }
                    this.setState({errorMessage: errorMessage});
                })
        } else {
            this.setState({errorMessage: 'Os campos e-mail e senha são obrigatórios'});
        }
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    renderErrorMessage() {
        const {errorMessage} = this.state;
        if (errorMessage) {
            return <div className="alert alert-danger">{this.state.errorMessage}</div>

        }
    }

    render () {
        const {username, password} = this.state;
        return (
            <div>
                {this.renderErrorMessage()}
                <h2>Login</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="form-group col-sm-4">
                            <input
                                type="text"
                                onChange={this.handleChange}
                                name="username"
                                value={username}
                                className="form-control"
                                placeholder="E-mail"
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm-4">
                            <input
                                type="password"
                                onChange={this.handleChange}
                                name="password"
                                value={password}
                                className="form-control"
                                placeholder="Senha"
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm-4">
                            <button type="submit" className="btn btn-primary">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}


const STATE_CHOICES = [
    'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN',
    'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO', 'DF'
];
const CATEGORY_CHOICES = ['Telefone', 'Chat', 'Email'];
const MOTIVE_CHOICES = ['Dúvidas', 'Elogios', 'Sugestões'];


export class AttendantPage extends Component {
    constructor() {
        super();
        this.initialState = {
            state: '',
            category: '',
            motive: '',
            description: '',
            errors: []
        };
        this.state = this.initialState;
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        const ticket = this.getTicket();
        let errors = _.reduce(ticket, (result, value, key) => {
            if (!value) {
                result.push(key);
            }
            return result;
        }, []);
        if (errors.length) {
            this.setState({errors});
        } else {
            store.dispatch(saveTicket(this.state))
                .then(() => this.setState(this.initialState));
        }
    }

    componentWillMount() {
        store.dispatch(requestAttendantTickets());
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    setTicket(ticket) {
        this.setState({...ticket});
    }

    getTicket() {
        return _.omit(this.state, 'errors');
    }

    render() {
        const { tickets } = this.props;
        return (
            <div>
                <FormTicket
                    ticket={this.getTicket()}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                    errors={this.state.errors}
                />
                <table className="table">
                    <thead>
                        <tr>
                            <th>Estado</th>
                            <th>Categoria</th>
                            <th>Motivo</th>
                            <th>Descrição</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {tickets.map(ticket =>
                            <tr key={ticket.id}>
                                <td>{ticket.state}</td>
                                <td>{ticket.category}</td>
                                <td>{ticket.motive}</td>
                                <td>{ticket.description}</td>
                                <td>
                                    <a href="#" className="pull-right" onClick={this.setTicket.bind(this, ticket)}>
                                        <i style={{marginLeft: '5px'}} className="fa fa-edit"/>
                                    </a>
                                    <a href="#" className="pull-right" onClick={() => store.dispatch(deleteTicket(ticket))}>
                                        <i className="fa fa-remove"/>
                                    </a>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}

AttendantPage.propTypes = {
    tickets: PropTypes.array.isRequired
}


class FormTicket extends Component {
    hasError(input) {
        return _.includes(this.props.errors, input);
    }

    render() {
        const {state, category, motive, description} = this.props.ticket;
        return (
            <form onSubmit={this.props.handleSubmit}>
                <div className={'form-group col-sm-4' + (this.hasError('state') ? ' has-error' : '')}>
                    <select onChange={this.props.handleChange} name="state" value={state} className="form-control">
                        <option>Estado</option>
                        {STATE_CHOICES.map(state =>
                            <option key={state} value={state}>{state}</option>
                        )}
                    </select>
                    {this.hasError('state') ?
                        <small className="control-label">Este campo é obrigatório</small> : null}
                </div>
                <div className={'form-group col-sm-4' + (this.hasError('category') ? ' has-error' : '')}>
                    <select onChange={this.props.handleChange} name="category" value={category} className="form-control">
                        <option>Categoria</option>
                        {CATEGORY_CHOICES.map(category =>
                            <option key={category} value={category}>{category}</option>
                        )}
                    </select>
                    {this.hasError('category') ?
                        <small className="control-label">Este campo é obrigatório</small> : null}
                </div>
                <div className={'form-group col-sm-4' + (this.hasError('motive') ? ' has-error' : '')}>
                    <select onChange={this.props.handleChange} name="motive" value={motive} className="form-control">
                        <option>Motivo</option>
                        {MOTIVE_CHOICES.map(motive =>
                            <option key={motive} value={motive}>{motive}</option>
                        )}
                    </select>
                    {this.hasError('motive') ?
                        <small className="control-label">Este campo é obrigatório</small> : null}
                </div>
                <div className={'form-group col-sm-12' + (this.hasError('description') ? ' has-error' : '')}>
                    <textarea
                        type="text"
                        onChange={this.props.handleChange}
                        name="description"
                        value={description}
                        className="form-control"
                        placeholder="Descrição"
                    />
                    {this.hasError('description') ?
                        <small className="control-label">Este campo é obrigatório</small> : null}
                </div>
                <div className="form-group col-sm-4">
                    <button type="submit" className="btn btn-primary">Salvar</button>
                </div>
            </form>
        )
    }
}

FormTicket.propTypes = {
    ticket: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired
}


export class TicketsPage extends Component {
    constructor() {
        super();
        this.renderGroupedTicketsByDate = this.renderGroupedTicketsByDate.bind(this);
    }

    componentWillMount() {
        store.dispatch(requestTickets());
    }

    renderGroupedTicketsByDate(tickets, date) {
        return (
            <Collapse title={date}>
                <GroupedItems
                    items={tickets}
                    iteratee={ticket => ticket.state}
                    renderItems={this.renderGroupedTicketsByState}
                />
            </Collapse>
        )
    }

    renderGroupedTicketsByState(tickets, state) {
        return (
            <Collapse title={state}>
                <table className="table">
                    <tbody>
                        {tickets.map(ticket =>
                            <tr key={ticket.id}>
                                <td width="100px">{ticket.category}</td>
                                <td width="100px">{ticket.motive}</td>
                                <td>{ticket.description}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </Collapse>
        )
    }

    render() {
        const {tickets} = this.props;
        return (
            <div>
                <h2>Atendimentos</h2>
                <GroupedItems
                    items={tickets}
                    iteratee={ticket => ticket.formatted_date}
                    renderItems={this.renderGroupedTicketsByDate}
                />
            </div>
        )
    }
}

TicketsPage.propTypes = {
    tickets: PropTypes.array.isRequired,
}


class GroupedItems extends Component {
    groupedItems() {
        const {items, iteratee} = this.props;
        return _.groupBy(items, iteratee);
    }

    render() {
        return (
            <ul style={{listStyleType: 'none', paddingLeft: 0}}>
                {_.map(this.groupedItems(), (items, key) =>
                    <li key={key}>
                        {this.props.renderItems(items, key)}
                    </li>
                )}
            </ul>
        )
    }
}

GroupedItems.propTypes = {
    items: PropTypes.array.isRequired,
    iteratee: PropTypes.func.isRequired,
    renderItems: PropTypes.func.isRequired
}


class Collapse extends Component {
    constructor() {
        super();
        this.state = {
            show: true
        };
        this.toogle = this.toogle.bind(this);
    }

    toogle() {
        this.setState({show: !this.state.show});
    }

    render() {
        const {title, children} = this.props;
        const {show} = this.state;
        return (
            <div>
                <a href="#" onClick={this.toogle} style={{textDecoration: 'none'}}>
                    <i className={show ? 'fa fa-plus' : 'fa fa-minus'} style={{marginRight: '10px'}} /> {title}
                </a>
                <div style={{marginLeft: '25px'}}>
                    {show ? children : null}
                </div>
            </div>
        )
    }
}

Collapse.propTypes = {
    title: PropTypes.node.isRequired,
    children: PropTypes.node.isRequired
}
