import React, {Component} from 'react';

import store from './store';
import {logout, changePage} from './actions';
import {AttendantPage, LoginPage, TicketsPage} from './components';


export default class App extends Component {
    constructor() {
        super()
        this.state = store.getState()
        this.unsubscribe = store.subscribe(this.changeState.bind(this))
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    changeState() {
        this.setState(store.getState());
    }

    userIsAuthenticated() {
        return Boolean(this.state.user.token);
    }

    renderPage() {
        const {activePage, tickets} = this.state;
        if (this.userIsAuthenticated()) {
            if (activePage == 'tickets') {
                return <TicketsPage tickets={tickets} />;
            } else {
                return <AttendantPage tickets={tickets} />;
            }
        }
        return <LoginPage />;
    }

    renderLogoutButton() {
        if (this.userIsAuthenticated()) {
            return (
                <li>
                    <a href="#" onClick={() => store.dispatch(logout())}>
                        Logout
                    </a>
                </li>
            );
        }
    }

    render() {
        const {activePage} = this.state;
        return (
            <div>
                <nav className="navbar navbar-inverse navbar-fixed-top">
                    <div className="container">
                        <div className="navbar-collapse collapse">
                            <ul className="nav navbar-nav">
                                <li className={activePage != 'tickets' ? 'active' : ''}>
                                    <a href="#" onClick={() => store.dispatch(changePage('attendant'))}>
                                        Atendente
                                    </a>
                                </li>
                                <li className={activePage == 'tickets' ? 'active' : ''}>
                                    <a href="#" onClick={() => store.dispatch(changePage('tickets'))}>
                                        Atendimentos
                                    </a>
                                </li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                {this.renderLogoutButton()}
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container" style={{marginTop: '80px'}}>
                    {this.renderPage()}
                </div>
            </div>
        )
    }
}
