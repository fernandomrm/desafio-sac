import request from './requests';


export const RECEIVE_TICKETS = 'RECEIVE_TICKETS';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const CHANGE_PAGE = 'CHANGE_PAGE';

export function requestTickets() {
    return dispatch => {
        return request('/tickets/')
            .then(tickets => dispatch(receiveTickets(tickets)));
    }
}

export function requestAttendantTickets() {
    return dispatch => {
        return request('/attendant-tickets/')
            .then(tickets => dispatch(receiveTickets(tickets)));
    }
}

function receiveTickets(tickets) {
    return {
        type: RECEIVE_TICKETS,
        tickets
    }
}

export function saveTicket(ticket) {
    return dispatch => {
        let method = ticket.id ? 'PUT' : 'POST';
        let url = '/tickets/' + (ticket.id ? ticket.id + '/' : '');
        return request(url, method, ticket)
            .then(tickets => dispatch(requestTickets()));
    }
}

export function deleteTicket(ticket) {
    return dispatch => {
        return request('/tickets/' + ticket.id + '/', 'DELETE')
            .then(() => dispatch(requestTickets()));
    }
}

export function login(credentials) {
    return dispatch => {
        return request('/login/', 'POST', credentials)
            .then(user => {
                localStorage.setItem('user', JSON.stringify(user));
                dispatch({
                    type: LOGIN,
                    user
                });
            });
    }
}

export function logout() {
    return dispatch => {
        return request('/logout/', 'POST')
            .then(() => {
                localStorage.removeItem('user');
                dispatch({type: LOGOUT});
            });
    }
}

export function changePage(page) {
    return {
        type: CHANGE_PAGE,
        page
    }
}
