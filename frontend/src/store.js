import thunk from 'redux-thunk';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {tickets, user, activePage} from './reducers';


const initialState = {
    user: JSON.parse(localStorage.getItem('user')) || {}
}
const rootReducer = combineReducers({tickets, user, activePage});
export default createStore(rootReducer, initialState, applyMiddleware(thunk));
