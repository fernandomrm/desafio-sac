import expect from 'expect';

import {RECEIVE_TICKETS, LOGIN, LOGOUT, CHANGE_PAGE} from '../src/actions';
import {tickets, user, activePage} from '../src/reducers';


const _tickets = [{
    id: 1,
    category: 'Telefone',
    state: 'RJ',
    motive: 'Dúvidas',
    description: 'Description'
}];
const _user = {token: 'token'};

describe('Reducers', () => {
    it('Tickets reducer return initial state', () => {
        expect(tickets(undefined, {})).toEqual([]);
    });

    it('Tickets reducer change state with RECEIVE_TICKETS action', () => {
        expect(
            tickets([], {
                type: RECEIVE_TICKETS,
                tickets: _tickets
            })
        ).toEqual(_tickets);
    });

    it('User reducer return initial state', () => {
        expect(user(undefined, {})).toEqual({});
    });

    it('User reducer change state with LOGIN action', () => {
        expect(
            user({}, {
                type: LOGIN,
                user: _user
            })
        ).toEqual(_user);
    });

    it('User reducer change state with LOGOUT action', () => {
        expect(
            user(_user, {type: LOGOUT})
        ).toEqual({});
    });

    it('Active page reducer return initial state', () => {
        expect(activePage(undefined, {})).toEqual('');
    });

    it('Active page reducer change with CHANGE_PAGE action', () => {
        expect(
            activePage('', {type: CHANGE_PAGE, page: 'tickets'})
        ).toEqual('tickets');
    });
});
