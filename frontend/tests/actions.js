import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';
import nock from 'nock';

import * as actions from '../src/actions';


const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const _tickets = [{
    id: 1,
    category: 'Telefone',
    state: 'RJ',
    motive: 'Dúvidas',
    description: 'Description'
}];

const _ticket = {
    category: 'Telefone',
    state: 'RJ',
    motive: 'Dúvidas',
    description: 'Description'
}

describe('Actions', () => {
    afterEach(() => {
        nock.cleanAll()
    });

    it('Action request tickets', () => {
        nock('http://localhost:8000/')
            .get('/tickets/')
            .reply(200, _tickets);

        const expectedActions = [{
            type: actions.RECEIVE_TICKETS,
            tickets: _tickets
        }];
        const store = mockStore();

        return store.dispatch(actions.requestTickets())
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action request attendant tickets', () => {
        nock('http://localhost:8000/')
            .get('/attendant-tickets/')
            .reply(200, _tickets);

        const expectedActions = [{
            type: actions.RECEIVE_TICKETS,
            tickets: _tickets
        }];
        const store = mockStore();

        return store.dispatch(actions.requestAttendantTickets())
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action save ticket', () => {
        nock('http://localhost:8000/')
            .post('/tickets/')
            .reply(201, _ticket);

        nock('http://localhost:8000/')
            .get('/tickets/')
            .reply(200, _tickets);

        const expectedActions = [{
            type: actions.RECEIVE_TICKETS,
            tickets: _tickets
        }];
        const store = mockStore();

        return store.dispatch(actions.saveTicket(_ticket))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action delete ticket', () => {
        var ticket = {..._tickets, id: 1};

        nock('http://localhost:8000/')
            .delete('/tickets/1/')
            .reply(204);

        nock('http://localhost:8000/')
            .get('/tickets/')
            .reply(200, []);

        const expectedActions = [{
            type: actions.RECEIVE_TICKETS,
            tickets: []
        }];
        const store = mockStore();

        return store.dispatch(actions.deleteTicket(ticket))
              .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
              });
    });

    it('Action login', () => {
        let _user = {token: 'token'};
        nock('http://localhost:8000/')
            .post('/login/')
            .reply(200, _user);

        const expectedActions = [{
            type: actions.LOGIN,
            user: _user
        }]
        const store = mockStore();

        return store.dispatch(actions.login({username: 'username', password: 'password'}))
            .then(() => {
                  expect(store.getActions()).toEqual(expectedActions)
            });
    });

    it('Action logout', () => {
        const expectedActions = [{
            type: actions.LOGOUT,
        }]
        nock('http://localhost:8000/')
            .post('/logout/')
            .reply(204);
        const store = mockStore();

        return store.dispatch(actions.logout())
            .then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
    });

    it('Action change page', () => {
        const expectedActions = [{
            type: actions.CHANGE_PAGE,
            page: 'tickets'
        }]
        const store = mockStore();

        store.dispatch(actions.changePage('tickets'));
        expect(store.getActions()).toEqual(expectedActions);
    });
});
